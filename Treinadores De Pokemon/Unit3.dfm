object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Inserir'
  ClientHeight = 288
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 40
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 32
    Top = 128
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 36
    Top = 207
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object DBEdit1: TDBEdit
    Left = 112
    Top = 37
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 0
  end
  object button1: TButton
    Left = 128
    Top = 239
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 1
    OnClick = button1Click
  end
  object DBEdit3: TDBEdit
    Left = 112
    Top = 204
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 2
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 112
    Top = 128
    Width = 145
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule2.DataSourcePokemon
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule2.DataSourceTreinador
    TabOrder = 3
  end
end
