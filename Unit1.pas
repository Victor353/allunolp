unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, unit2,
  System.Net.URLClient, System.Net.HttpClient, System.Net.HttpClientComponent,
  Vcl.Menus;

type
  TForm1 = class(TForm)
    Button1: TButton;
    MainMenu1: TMainMenu;
    Aplicativo1: TMenuItem;
    Dowload1: TMenuItem;
    Memo1: TMemo;
    navegador: TNetHTTPClient;
    procedure Button1Click(Sender: TObject);
    procedure Dowload1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
 Form2 := TForm2.Create(Application);
 Form2.Visible := True;
end;

procedure TForm1.Dowload1Click(Sender: TObject);
var conteudo : string;
begin
    conteudo := navegador.Get('https://venson.net.br').ContentAsString();
    Memo1.Text := conteudo;
end;

end.
